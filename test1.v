//
// Simple UART test driver
// read a character, increment and send it back
//
module test1(clock,from2102,to2102);
	input clock,from2102;
	output to2102;

	wire [7:0]rcvword;
	reg [7:0]xword;
  reg rcvread,xsend;
  wire rcvready,xready;

	UART myUART(clock,from2102,rcvready,rcvread,rcvword,to2102,xready,xsend,xword);	// instance of UART

	always @(negedge clock) begin
		if (rcvready) begin		// incoming character ready
			rcvread<=1;					// show we've read it
			xword<=rcvword+1;			// copy to the output buffer
			xsend<=1;						// and initiate transmission
		end else begin
			rcvread<=0;					// clear our read flag
			if (~xready) xsend<=0;	// clear send request once transmission has started
		end

	end
endmodule