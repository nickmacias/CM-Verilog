// basic cell definition
module cmcell(sysclock,dn,ds,dw,de,cn,cs,cw,ce,reset,clock,dno,dso,dwo,deo,cno,cso,cwo,ceo,membitnum,membit);
	input sysclock,dn,ds,dw,de,cn,cs,cw,ce,reset,clock;
	input [6:0] membitnum;
	output dno,dso,dwo,deo,cno,cso,cwo,ceo,membit;

	wire [6:0] membitnum;

	wire dno,dso,dwo,deo,cno,cso,cwo,ceo; // avoid warnings
	reg sdno,sdso,sdwo,sdeo,scno,scso,scwo,sceo;	// synchronous outputs

  assign membit=mem[membitnum[6:3]][membitnum[2:0]];

// internal memory
  reg[7:0] mem[0:15];

// bit counter
	reg [6:0] counter;
	reg[6:0] oldcounter;

wire mode;	// 1=C-mode
integer i,j; // misc indexes
wire [3:0]bitword;
reg savebit;	//latched bit from rising clock edge
reg wasincmode; // set if we were in C-mode on last pos edge

wire [3:0] row,oldrow;
wire [2:0] col,oldcol;

assign mode=(cn|cs|cw|ce);	// C-mode
	//assign mode=(cn==1 || cs==1 || cw==1 || ce==1)?1:0;
// Bit ordering in TT (where c6 c5 c4 c3 c2 c1 c0 is the bit #) is:
//	row=~c5 ~c4 ~c3 ~c2
//	col=~c6 c1 c0
assign row[0]=~counter[2];
assign row[1]=~counter[3];
assign row[2]=~counter[4];
assign row[3]=~counter[5];
///assign row=!{counter[5],counter[4],counter[3],counter[2]};
assign col[0]=counter[0];
assign col[1]=counter[1];
assign col[2]=~counter[6];
//assign col={counter[6],counter[1],counter[0]}^3'b011;
assign oldrow[0]=~oldcounter[2];
assign oldrow[1]=~oldcounter[3];
assign oldrow[2]=~oldcounter[4];
assign oldrow[3]=~oldcounter[5];
///assign oldrow=~{oldcounter[5],oldcounter[4],oldcounter[3],oldcounter[2]};
assign oldcol[0]=oldcounter[0];
assign oldcol[1]=oldcounter[1];
assign oldcol[2]=~oldcounter[6];
//assign oldcol={oldcounter[6],oldcounter[1],oldcounter[0]}^3'b011;

// assign outputs
/***
assign cno=mode ? 1'b0:mem[{dw,ds,de,dn}][3];
assign cso=mode ? 1'b0:mem[{dw,ds,de,dn}][1]; // *** seems as if when e.g. de changes, there may be a glitch in the output...***
assign cwo=mode ? 1'b0:mem[{dw,ds,de,dn}][0];
assign ceo=mode ? 1'b0:mem[{dw,ds,de,dn}][2];
assign dno=mode ? mem[row][col]&cn : mem[{dw,ds,de,dn}][7];
assign dso=mode ? mem[row][col]&cs : mem[{dw,ds,de,dn}][5];
assign dwo=mode ? mem[row][col]&cw : mem[{dw,ds,de,dn}][4];
assign deo=mode ? mem[row][col]&ce : mem[{dw,ds,de,dn}][6];
***/

assign dno=sdno;
assign dso=sdso;
assign dwo=sdwo;
assign deo=sdeo;
assign cno=scno;
assign cso=scso;
assign cwo=scwo;
assign ceo=sceo;

always @(posedge sysclock) begin  // lame synchronous operation :(
	scno<=mode ? 1'b0:mem[{dw,ds,de,dn}][3];
	scso<=mode ? 1'b0:mem[{dw,ds,de,dn}][1]; // *** seems as if when e.g. de changes, there may be a glitch in the output...***
	scwo<=mode ? 1'b0:mem[{dw,ds,de,dn}][0];
	sceo<=mode ? 1'b0:mem[{dw,ds,de,dn}][2];
	sdno<=mode ? mem[row][col]&cn : mem[{dw,ds,de,dn}][7];
	sdso<=mode ? mem[row][col]&cs : mem[{dw,ds,de,dn}][5];
	sdwo<=mode ? mem[row][col]&cw : mem[{dw,ds,de,dn}][4];
	sdeo<=mode ? mem[row][col]&ce : mem[{dw,ds,de,dn}][6];
end

always @(negedge clock,/***negedge mode,***/posedge reset) begin // works better with mode edge removed (but not right)
	if (reset) begin
		mem[0]<=8'b00000000;
		mem[1]<=8'b00000000;
		mem[2]<=8'b00000000;
		mem[3]<=8'b00000000;
		mem[4]<=8'b00000000;
		mem[5]<=8'b00000000;
		mem[6]<=8'b00000000;
		mem[7]<=8'b00000000;
		mem[8]<=8'b00000000;
		mem[9]<=8'b00000000;
		mem[10]<=8'b00000000;
		mem[11]<=8'b00000000;
		mem[12]<=8'b00000000;
		mem[13]<=8'b00000000;
		mem[14]<=8'b00000000;
		mem[15]<=8'b00000000;

		counter<=7'b0000000;
	end
	else if (~mode) begin
		counter<=0;
	end
	else if (wasincmode) begin	// only do this is we were already in c-mode on previous positive clock edge
		counter<=counter+7'b1;
		mem[oldrow][oldcol] <= savebit; // write latched bit into mem
	end
end

always @(posedge clock) begin
	wasincmode<=mode; // remember if we were already in c-mode
	savebit<=(cn&dn) | (cs&ds) | (cw&dw) | (ce&de); // this will get shifted in on the falling edge
	oldcounter<=counter;	// write to here next time
end
endmodule