//
// Better CM driver. Break the display into zones around each cell's
// display area, and track these zones during the hpos/hline update codes.
// This should be more HW-efficient than calculating zones from hpos/hline,
// especially when dimensions aren't integral powers of 2...
//
// Read key commands and modulate a cell's inputs.
// Meanwhile, display on the VGA :)
//
module cmtest2(clock,from2102,to2102,red,green,blue,hsync,vsync);

// VGA vars
  output red, green, blue, hsync, vsync;
  wire clock;
  reg [4:0] red;
  reg [3:0] green;
  reg [4:0] blue;
  reg hsync;
	reg vsync;

	reg [12:0] hpos;	// #ticks in current hline (=hline time*50)
	reg  [9:0] hline; // line #(includes blanks and syncs)
  reg [25:0] ttime; // #ticks since first visible line/first visible pixel

// VGA zone variables
	reg [4:0] hpix,vpix;		// largest zone=16 pixels
	reg [4:0] hzone,vzone;	// what zone are we in?

	reg [4:0] hcell,vcell;	// cell #s (column, row)

// CM vars
	reg dn,ds,dw,de,cn,cs,cw,ce,cmreset,cmclock;
	wire membit;
	wire[6:0]membitnum;	// choose bit to access

	reg[1:0] side; // 0=n, 1=s, 2=w, 3=e
	reg cd;				// 0=d, 1=c


// Other vars
	input clock,from2102;
	output to2102;

	wire [7:0]rcvword;
	reg [7:0]xword;
  reg rcvread,xsend;
  wire rcvready,xready;

	reg [7:0] a;

	UART myUART(clock,from2102,rcvready,rcvread,rcvword,to2102,xready,xsend,xword);	// instance of UART

// instantiate cells
	cmcell CELL00(dn,ds,dw,de,cn,cs,cw,ce,cmreset,cmclock,dno,dso,dwo,deo,cno,cso,cwo,ceo,membitnum,membit);

	always @(negedge clock) begin
		if (xsend) xsend<=0; // we started transmission of a word, so clear our send request
		else if (rcvready) begin
// send an ack
			xword[7:0]<=rcvword[7:0];
//			xword<="K";	// echo back
			xsend<=1;

// process the incoming character
			rcvread<=1;	// grab the word

// parse the command
			if (rcvword=="N") side<=0;
			else if (rcvword=="S") side<=1;
			else if (rcvword=="W") side<=2;
			else if (rcvword=="E") side<=3;
			else if (rcvword=="D") cd<=0;
			else if (rcvword=="C") cd<=1;
			else if (rcvword=="t") cmclock<=1;
			else if (rcvword=="y") cmclock<=0;
			else if (rcvword=="R") cmreset<=1;
			else if (rcvword=="r") begin
				cmreset<=0;
				dn<=0;ds<=0;dw<=0;de<=0;cn<=0;cs<=0;cw<=0;ce<=0;
			end else if (rcvword=="1") begin
				case ((cd<<2)|side)
					0: dn<=1;
					1: ds<=1;
					2: dw<=1;
					3: de<=1;
					4: cn<=1;
					5: cs<=1;
					6: cw<=1;
					7: ce<=1;
				endcase
			end else if (rcvword=="0") begin
				case ((cd<<2)|side)
					0: dn<=0;
					1: ds<=0;
					2: dw<=0;
					3: de<=0;
					4: cn<=0;
					5: cs<=0;
					6: cw<=0;
					7: ce<=0;
				endcase
			end
		end else begin
			rcvread<=0;
		end
	end

// VGA interface
// one clock tick=20 nSec=0.02 uSec (50 ticks per uS)

	//assign membitnum=((hline-128)&7'b1111000) | (((hpos-128)&6'b111000)>>3);
	assign membitnum=((vzone-2)<<3) | (hzone-2);
  always @(negedge clock) begin
	
		if (hpos > 1033) begin	// end of hsweep
			hpos<=0;
			hzone<=0;hpix<=0;
			hcell<=0;
			if (hline > 806) begin	// end of frame
				hline<=0;
				vzone<=0;vpix<=0;
				vcell<=0;
			end else begin
				hline<=hline+1;
				// adjust vertical zone and pixel
				if (vpix==7) begin
					vpix<=0;
					if (vzone==19) begin
						vzone<=0;
						vcell<=vcell+1;
					end	else vzone<=vzone+1;
				end else vpix<=vpix+1;
			end
		end else begin
			hpos<=hpos+1;
			// adjust horizontal zone and pixel
			if (hpix==7) begin
				hpix<=0;
				if (hzone==11) begin
					hzone<=0;
					hcell<=hcell+1;
				end	else hzone<=hzone+1;
			end else hpix<=hpix+1;
		end

// now drive syncs and bits based on hline/hpos

// hsync first
		if ((hpos >= 806) && (hpos < 910)) begin	// hsync
			hsync <= 0;
		end else begin
			hsync <= 1;
		end

// vsync
		if ((hline >= 771) && (hline < 777)) begin	// vsync
			vsync<=0;
		end else begin
			vsync<=1;
		end

// set databits here
		if (hline >= 768) begin
				red<=0;green<=0;blue<=0;
		end else if (hpos > 787) begin
				red<=0;green<=0;blue<=0;
		end else begin

// START OF PIXEL DRAWING
// Go crazy! Lite some pixels!

// make some assignments so we know cell-row, cell-col, and border/TT region

// edges: outputs
			if (hzone >= 2 && hzone <= 3 && vzone>=0 && vzone<=1) begin
				red<=0;green<=0;
				//blue<=dno?31:0;
				blue<=(dno && (hpix+(hzone==3?8:0))>=8-((vpix+(vzone==1?8:0))>>1) &&
								(hpix+(hzone==3?8:0))<=8+((vpix+(vzone==1?8:0))>>1))?31:0;
			end else if (hzone>=4 && hzone<=5 && vzone>=0 && vzone<=1) begin
				blue<=0;green<=0;
				//red<=cno?31:0;
				red<=(cno && (hpix+(hzone==5?8:0))>=8-((vpix+(vzone==1?8:0))>>1) &&
							(hpix+(hzone==5?8:0))<=8+((vpix+(vzone==1?8:0))>>1))?31:0;
			end else if (hzone>=6 && hzone<=7 && vzone>=18 && vzone<=19) begin
				red<=0;green<=0;
				blue<=dso?31:0;
			end else if (hzone>=8 && hzone<=9 && vzone>=18 && vzone<=19) begin
				blue<=0;green<=0;
				red<=cso?31:0;
			end else if (hzone>=0 && hzone<=1 && vzone>=14 && vzone<=15) begin
				red<=0;green<=0;
				blue<=dwo?31:0;
			end else if (hzone>=0 && hzone<=1 && vzone>=16 && vzone<=17) begin
				blue<=0;green<=0;
				red<=cwo?31:0;
			end else if (hzone>=10 && hzone<=11 && vzone>=2 && vzone<=3) begin
				red<=0;green<=0;
				blue<=deo?31:0;
			end else if (hzone>=10 && hzone<=11 && vzone>=4 && vzone<=5) begin
				blue<=0;green<=0;
				red<=ceo?31:0;

// TT bits
			end else if (hzone >= 2 && hzone <= 9 && vzone>=2 && vzone<=17) begin
				//membitnum<=((hline-128)&7'b1111000) | (((hpos-128)&6'b111000)>>3);
				blue<=0;
				red<=0;
				green<=membit?15:0;	// if we say membitnum<= then we'll be using the old membit here...
			end else begin // off-grid
				blue<=0;
				red<=0;
				green<=0;
			end;

// END OF PIXEL DRAWING
		end
  end // end of clock tick always() block
  
endmodule