//
// Better CM driver. Break the display into zones around each cell's
// display area, and track these zones during the hpos/hline update codes.
// This should be more HW-efficient than calculating zones from hpos/hline,
// especially when dimensions aren't integral powers of 2...
//
// Read key commands and modulate a cell's inputs.
// Meanwhile, display on the VGA :)
//
module cmtest6(clock,from2102,to2102,red,green,blue,hsync,vsync,chg);
reg [2:0]chg;
output [2:0]chg;

	parameter rows=4,cols=8;	// size of matrix
	//parameter rows=2,cols=3;

// VGA vars
  output [4:0]red,blue;
	output [3:0]green;
	output  hsync, vsync;
  wire clock;
  reg [4:0] red;
  reg [3:0] green;
  reg [4:0] blue;
  reg hsync;
	reg vsync;

	reg [12:0] hpos;	// #ticks in current hline (=hline time*50)
	reg  [9:0] hline; // line #(includes blanks and syncs)
  reg [25:0] ttime; // #ticks since first visible line/first visible pixel

// VGA zone variables
	reg [4:0] hpix,vpix;		// largest zone=16 pixels
	reg [4:0] hzone,vzone;	// what zone are we in?

	reg [4:0] hcell,vcell;	// cell #s (column, row)

// CM I/O and control
	reg dn[rows-1:0][cols-1:0],ds[rows-1:0][cols-1:0],dw[rows-1:0][cols-1:0],de[rows-1:0][cols-1:0];
	reg cn[rows-1:0][cols-1:0],cs[rows-1:0][cols-1:0],cw[rows-1:0][cols-1:0],ce[rows-1:0][cols-1:0];
	reg cmreset,cmclock;
	wire dno[rows-1:0][cols-1:0],dso[rows-1:0][cols-1:0],dwo[rows-1:0][cols-1:0],deo[rows-1:0][cols-1:0];
	wire cno[rows-1:0][cols-1:0],cso[rows-1:0][cols-1:0],cwo[rows-1:0][cols-1:0],ceo[rows-1:0][cols-1:0];

	wire membit[rows-1:0][cols-1:0];
	wire[6:0]membitnum;	// choose bit to access

// Parser vars
	reg[1:0] side; // 0=n, 1=s, 2=w, 3=e
	reg cd;				// 0=d, 1=c
	reg [2:0] iorow,iocol; // which cell are we addressing?


// Other vars
	input clock,from2102;
	output to2102;

	wire [7:0]rcvword;
	reg [7:0]xword;
  reg rcvread,xsend;
  wire rcvready,xready;

	reg [7:0] a;

//always @(negedge ceo[0][1]) begin
//  chg<=chg+1;
//end

	UART myUART(clock,from2102,rcvready,rcvread,rcvword,to2102,xready,xsend,xword);	// instance of UART

// instantiate cells
/***
	cmcell CELL00(dn[0],ds[0],dw[0],dwo[1],cn[0],cs[0],cw[0],cwo[1],
                cmreset,cmclock,dno[0],dso[0],dwo[0],deo[0],cno[0],cso[0],cwo[0],ceo[0],membitnum,membit[0]);

	cmcell CELL01(dn[1],ds[1],deo[0],de[1],cn[1],cs[1],ceo[0],ce[1],
                cmreset,cmclock,dno[1],dso[1],dwo[1],deo[1],cno[1],cso[1],cwo[1],ceo[1],membitnum,membit[1]);
***/

// interior
	generate
	genvar row,col;
	for (row=1;row<rows-1;row=row+1) begin: cmgenrow
		for (col=1;col<cols-1;col=col+1) begin: cmgencol
		cmcell CELL_row_col(clock,dso[row-1][col],dno[row+1][col],deo[row][col-1],dwo[row][col+1],
                    cso[row-1][col],cno[row+1][col],ceo[row][col-1],cwo[row][col+1],
											 cmreset,cmclock,
											 dno[row][col],dso[row][col],dwo[row][col],deo[row][col],
											 cno[row][col],cso[row][col],cwo[row][col],ceo[row][col],
											 membitnum,membit[row][col]);
		end
	end
	endgenerate

// top row
	generate
	for (col=1;col<cols-1;col=col+1) begin: cmgentop
		cmcell CELL_0_col(clock,dn[0][col],dno[1][col],deo[0][col-1],dwo[0][col+1],
                      cn[0][col],cno[1][col],ceo[0][col-1],cwo[0][col+1],
                      cmreset,cmclock,
                      dno[0][col],dso[0][col],dwo[0][col],deo[0][col],
                      cno[0][col],cso[0][col],cwo[0][col],ceo[0][col],
                      membitnum,membit[0][col]);
	end
	endgenerate	

// left column
	generate
	for (row=1;row<rows-1;row=row+1) begin: cmgenleft
		cmcell CELL_row_0(clock,dso[row-1][0],dno[row+1][0],dw[row][0],dwo[row][1],
                      cso[row-1][0],cno[row+1][0],cw[row][0],cwo[row][1],
                      cmreset,cmclock,
                      dno[row][0],dso[row][0],dwo[row][0],deo[row][0],
                      cno[row][0],cso[row][0],cwo[row][0],ceo[row][0],
                      membitnum,membit[row][0]);
	end
	endgenerate

// right column
	generate
	for (row=1;row<rows-1;row=row+1) begin: cmgenright
		cmcell CELL_row_n(clock,dso[row-1][cols-1],dno[row+1][cols-1],deo[row][cols-2],de[row][cols-1],
                      cso[row-1][cols-1],cno[row+1][cols-1],ceo[row][cols-2],ce[row][cols-1],
                      cmreset,cmclock,
                      dno[row][cols-1],dso[row][cols-1],dwo[row][cols-1],deo[row][cols-1],
                      cno[row][cols-1],cso[row][cols-1],cwo[row][cols-1],ceo[row][cols-1],
                      membitnum,membit[row][cols-1]);
	end
	endgenerate

// bottom row
	generate
	for (col=1;col<cols-1;col=col+1) begin: cmgenbot
		cmcell CELL_n_col(clock,dso[rows-2][col],ds[rows-1][col],deo[rows-1][col-1],dwo[rows-1][col+1],
                      cso[rows-2][col],cs[rows-1][col],ceo[rows-1][col-1],cwo[rows-1][col+1],
                      cmreset,cmclock,
                      dno[rows-1][col],dso[rows-1][col],dwo[rows-1][col],deo[rows-1][col],
                      cno[rows-1][col],cso[rows-1][col],cwo[rows-1][col],ceo[rows-1][col],
                      membitnum,membit[rows-1][col]);
	end
	endgenerate	

// upper-left cell
		cmcell CELL_UL(clock,dn[0][0],dno[1][0],dw[0][0],dwo[0][1],
                    cn[0][0],cno[1][0],cw[0][0],cwo[0][1],
											 cmreset,cmclock,
											 dno[0][0],dso[0][0],dwo[0][0],deo[0][0],
											 cno[0][0],cso[0][0],cwo[0][0],ceo[0][0],
											 membitnum,membit[0][0]);
// upper-right
		cmcell CELL_UR(clock,dn[0][cols-1],dno[1][cols-1],deo[0][cols-2],de[0][cols-1],
                   cn[0][cols-1],cno[1][cols-1],ceo[0][cols-2],ce[0][cols-1],
											 cmreset,cmclock,
											 dno[0][cols-1],dso[0][cols-1],dwo[0][cols-1],deo[0][cols-1],
											 cno[0][cols-1],cso[0][cols-1],cwo[0][cols-1],ceo[0][cols-1],
											 membitnum,membit[0][cols-1]);

// lower-left cell
		cmcell CELL_LL(clock,dso[rows-2][0],ds[rows-1][0],dw[rows-1][0],dwo[rows-1][1],
                   cso[rows-2][0],cs[rows-1][0],cw[rows-1][0],cwo[rows-1][1],
											 cmreset,cmclock,
											 dno[rows-1][0],dso[rows-1][0],dwo[rows-1][0],deo[rows-1][0],
											 cno[rows-1][0],cso[rows-1][0],cwo[rows-1][0],ceo[rows-1][0],
											 membitnum,membit[rows-1][0]);

// lower-right
		cmcell CELL_LR(clock,dso[rows-2][cols-1],ds[rows-1][cols-1],deo[rows-1][cols-2],de[rows-1][cols-1],
                   cso[rows-2][cols-1],cs[rows-1][cols-1],ceo[rows-1][cols-2],ce[rows-1][cols-1],
											 cmreset,cmclock,
											 dno[rows-1][cols-1],dso[rows-1][cols-1],dwo[rows-1][cols-1],deo[rows-1][cols-1],
											 cno[rows-1][cols-1],cso[rows-1][cols-1],cwo[rows-1][cols-1],ceo[rows-1][cols-1],
											 membitnum,membit[rows-1][cols-1]);

	always @(negedge clock) begin
		if (xsend && ~xready) xsend<=0; // we started transmission of a word, so clear our send request
// Seems we need to and with ~xready, otherwise the send may get cleared too quickly?
		else if (rcvready) begin
// send an ack
			xword<=rcvword;	// echo back
			xsend<=1;

// process the incoming character
			rcvread<=1;	// grab the word

// parse the command
			if (rcvword=="N") side<=0;
			else if (rcvword=="S") side<=1;
			else if (rcvword=="W") side<=2;
			else if (rcvword=="E") side<=3;
			else if (rcvword=="D") cd<=0;
			else if (rcvword=="C") cd<=1;
			else if (rcvword=="t") cmclock<=1;
			else if (rcvword=="y") cmclock<=0;
			else if (rcvword=="R") cmreset<=1;
			else if (rcvword=="r") begin
				cmreset<=0;
					dw[0][0]<=0;cw[0][0]<=0;
					dw[1][0]<=0;cw[1][0]<=0;
					//dn[0][1]<=0;dn[0][2]<=0;dn[0][3]<=0;de[0][3]<=0;
					//cn[0][1]<=0;cn[0][2]<=0;cn[0][3]<=0;ce[0][3]<=0;
					dn[0][1]<=0;dn[0][2]<=0;
					cn[0][1]<=0;cn[0][2]<=0;

			end else if (rcvword=="a") begin
				iorow<=0;iocol<=0;
			end else if (rcvword=="b") begin
				iorow<=1;iocol<=0;
			end else if (rcvword=="H") begin
				case ((cd<<2)|side)
					0: dn[iorow][iocol]<=1;
					1: ds[iorow][iocol]<=1;
					2: dw[iorow][iocol]<=1;
					3: de[iorow][iocol]<=1;
					4: cn[iorow][iocol]<=1;
					5: cs[iorow][iocol]<=1;
					6: cw[iorow][iocol]<=1;
					7: ce[iorow][iocol]<=1;
				endcase
			end else if (rcvword=="L") begin
				case ((cd<<2)|side)
					0: dn[iorow][iocol]<=0;
					1: ds[iorow][iocol]<=0;
					2: dw[iorow][iocol]<=0;
					3: de[iorow][iocol]<=0;
					4: cn[iorow][iocol]<=0;
					5: cs[iorow][iocol]<=0;
					6: cw[iorow][iocol]<=0;
					7: ce[iorow][iocol]<=0;
				endcase
			end
		end else begin
			rcvread<=0;
		end
	end

// VGA interface
// one clock tick=20 nSec=0.02 uSec (50 ticks per uS)

	assign membitnum=((vzone-5'b1)<<3) | (hzone-5'b1);
  always @(negedge clock) begin
	
		if (hpos > 1033) begin	// end of hsweep
			hpos<=0;
			hzone<=0;hpix<=0;
			hcell<=0;
			if (hline > 806) begin	// end of frame
				hline<=0;
				vzone<=0;vpix<=0;
				vcell<=0;
			end else begin
				hline<=hline+10'b1;
				// adjust vertical zone and pixel
				if (vpix==7) begin
					vpix<=0;
					if (vzone==19) begin
						vzone<=0;
						vcell<=vcell+5'b1;
					end	else vzone<=vzone+5'b1;
				end else vpix<=vpix+5'b1;
			end
		end else begin
			hpos<=hpos+13'b1;
			// adjust horizontal zone and pixel
			if (hpix==7) begin
				hpix<=0;
				if (hzone==11) begin
					hzone<=0;
					hcell<=hcell+5'b1;
				end	else hzone<=hzone+5'b1;
			end else hpix<=hpix+5'b1;
		end

// now drive syncs and bits based on hline/hpos

// hsync first
		if ((hpos >= 806) && (hpos < 910)) begin	// hsync
			hsync <= 0;
		end else begin
			hsync <= 1;
		end

// vsync
		if ((hline >= 771) && (hline < 777)) begin	// vsync
			vsync<=0;
		end else begin
			vsync<=1;
		end

// set databits here
		if (hline >= 768) begin
				red<=0;green<=0;blue<=0;
		end else if (hpos > 787) begin
				red<=0;green<=0;blue<=0;
		end else begin

// START OF PIXEL DRAWING
// Go crazy! Lite some pixels!

if (hcell>=0 && hcell<cols && vcell>=0 && vcell<rows) begin
// border
			if ((hzone>=1 && hzone<=8 && vzone==1 && vpix==0) ||
			    (hzone>=1 && hzone<=8 && vzone==16 && vpix==7) ||
			    (vzone>=1 && vzone<=16 && hzone==1 && hpix==0) ||
			    (vzone>=1 && vzone<=16 && hzone==8 && hpix==7))
			begin
				red<=31;green<=15;blue<=31;
// edges: outputs
			end else if (hzone == 2 && vzone==0) begin
				red<=0;green<=0;
				blue<=(dno[vcell][hcell] && (hpix<<1)>7-vpix && (hpix<<1) < 7+vpix)?5'b11111:5'b0;	// funky arrows :-P
			end else if (hzone==3 && vzone==0) begin
				blue<=0;green<=0;
				red<=(cno[vcell][hcell] && (hpix<<1)>7-vpix && (hpix<<1) < 7+vpix)?5'b11111:5'b0;
			end else if (hzone==6 && vzone==17) begin
				red<=0;green<=0;
				blue<=(dso[vcell][hcell] && (hpix<<1)>vpix && (hpix<<1) < 14-vpix)?5'b11111:5'b0;
			end else if (hzone==7 && vzone==17) begin
				blue<=0;green<=0;
				red<=(cso[vcell][hcell] && (hpix<<1)>vpix && (hpix<<1) < 14-vpix)?5'b11111:5'b0;
			end else if (hzone==0 && vzone==14) begin
				red<=0;green<=0;
				blue<=(dwo[vcell][hcell] && (vpix<<1)>7-hpix && (vpix<<1)<7+hpix)?5'b11111:5'b0;
			end else if (hzone==0 && vzone==15) begin
				blue<=0;green<=0;
				red<=(cwo[vcell][hcell] && (vpix<<1)>7-hpix && (vpix<<1)<7+hpix)?5'b11111:5'b0;
			end else if (hzone==9 && vzone==2) begin
				red<=0;green<=0;
				blue<=(deo[vcell][hcell] && (vpix<<1)>hpix && (vpix<<1)<14-hpix)?5'b11111:5'b0;
			end else if (hzone==9 && vzone==3) begin
				blue<=0;green<=0;
				red<=(ceo[vcell][hcell] && (vpix<<1)>hpix && (vpix<<1)<14-hpix)?5'b11111:5'b0;

// TT bits
			end else if (hzone >= 1 && hzone <= 8 && vzone>=1 && vzone<=16) begin
				blue<=0;
				red<=0;
				green<=membit[vcell][hcell]?4'b1111:4'b0;	// if we say membitnum<= then we'll be using the old membit here...
			end else begin // off-grid
				blue<=0;
				red<=0;
				green<=0;
			end;
// END OF PIXEL DRAWING
end else begin red<=0;green<=0;blue<=0;end	// not a valid cell location

		end
  end // end of clock tick always() block
  
endmodule