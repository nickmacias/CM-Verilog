//
// Simple UART test driver
// single-digit mod-10 adder
//
module test2(clock,from2102,to2102);
	input clock,from2102;
	output to2102;

	wire [7:0]rcvword;
	reg [7:0]xword;
  reg rcvread,xsend;
  wire rcvready,xready;

  reg [7:0] sum;

	UART myUART(clock,from2102,rcvready,rcvread,rcvword,to2102,xready,xsend,xword);	// instance of UART

	always @(negedge clock) begin
		if (rcvready) begin		// incoming character ready
			rcvread<=1;					// show we've read it
			if (rcvword == 32) xword<=48;
			  else xword<=(xword+(rcvword-48))>57?xword+(rcvword-58):xword+(rcvword-48);
			xsend<=1;						// and initiate transmission
		end else begin
			rcvread<=0;					// clear our read flag
			if (~xready) xsend<=0;	// clear send request once transmission has started
		end

	end
endmodule