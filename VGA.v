module VGA(clock,red,green,blue,hsync,vsync);
  input clock;
  output [4:0]red,blue;
	output [3:0]green;
	output hsync, vsync;
  wire clock;
  reg [4:0] red;
  reg [3:0] green;
  reg [4:0] blue;
  reg hsync;
	reg vsync;

	reg [12:0] hpos;	// #ticks in current hline (=hline time*50)
	reg  [9:0] hline; // line #(includes blanks and syncs)
  reg [25:0] ttime; // #ticks since first visible line/first visible pixel
  
// one clock tick=20 nSec=0.02 uSec (50 ticks per uS)
  always @(negedge clock) begin
	
		if (hpos > 1033) begin	// end of hsweep
			hpos<=0;
			if (hline > 806) begin	// end of frame
				hline<=0;
			end else begin
				hline<=hline+1;
			end
		end else begin
			hpos<=hpos+1;
		end

// now drive syncs and bits based on hline/hpos

// hsync first
		if ((hpos >= 806) && (hpos < 910)) begin	// hsync
			hsync <= 0;
		end else begin
			hsync <= 1;
		end

// vsync
		if ((hline >= 771) && (hline < 777)) begin	// vsync
			vsync<=0;
		end else begin
			vsync<=1;
		end

// set databits here
		if (hline >= 768) begin
				red<=0;green<=0;blue<=0;
		end else if (hpos > 787) begin
				red<=0;green<=0;blue<=0;
		end else begin

// START OF PIXEL DRAWING
// Go crazy! Lite some pixels!

			if ((hpos > 250) && (hpos < 300)) begin
				red<=31;
				blue<=0;
			end else begin
				if (hpos[6] ^ hline[6]) begin
					green<=0;
					blue<=31;
				end else begin
					green<=15;
					blue<=0;
				end
			end

// END OF PIXEL DRAWING
		end
  end // end of clock tick always() block
  
endmodule