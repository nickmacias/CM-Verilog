//
// read 1's and 0's, and display as a shift register on the VGA display :)
//
module test3(clock,from2102,to2102,red,green,blue,hsync,vsync);

// VGA vars
  output red[4:0], green[3:0], blue[4:0], hsync, vsync;
  wire clock;
  reg [4:0] red;
  reg [3:0] green;
  reg [4:0] blue;
  reg hsync;
	reg vsync;

	reg [12:0] hpos;	// #ticks in current hline (=hline time*50)
	reg  [9:0] hline; // line #(includes blanks and syncs)
  reg [25:0] ttime; // #ticks since first visible line/first visible pixel


// Other vars
	input clock,from2102;
	output to2102;

	wire [7:0]rcvword;
	reg [7:0]xword;
  reg rcvread,xsend;
  wire rcvready,xready;

	reg [7:0] a;

	UART myUART(clock,from2102,rcvready,rcvread,rcvword,to2102,xready,xsend,xword);	// instance of UART

	always @(negedge clock) begin
		if (rcvready) begin
			a<={a[6],a[5],a[4],a[3],a[2],a[1],a[0],(rcvword==48)?1'b0:1'b1};	// save this digit
			rcvread<=1;
		end else begin
			rcvread<=0;
		end
	end

// VGA interface
// one clock tick=20 nSec=0.02 uSec (50 ticks per uS)
  always @(negedge clock) begin
	
		if (hpos > 1033) begin	// end of hsweep
			hpos<=0;
			if (hline > 806) begin	// end of frame
				hline<=0;
			end else begin
				hline<=hline+1;
			end
		end else begin
			hpos<=hpos+1;
		end

// now drive syncs and bits based on hline/hpos

// hsync first
		if ((hpos >= 806) && (hpos < 910)) begin	// hsync
			hsync <= 0;
		end else begin
			hsync <= 1;
		end

// vsync
		if ((hline >= 771) && (hline < 777)) begin	// vsync
			vsync<=0;
		end else begin
			vsync<=1;
		end

// set databits here
		if (hline >= 768) begin
				red<=0;green<=0;blue<=0;
		end else if (hpos > 787) begin
				red<=0;green<=0;blue<=0;
		end else begin

// START OF PIXEL DRAWING
// Go crazy! Lite some pixels!

			if ((hline > 300) && (hline< 364) &&
				  (hpos> 250) && (hpos< 250+64*8)) begin
				blue<=0;
				red<=31;
				green<=(a[(hpos-250)>>6])?15:0;
			end else begin
				blue<=31;
				red<=0;
				green<=0;
			end;

// END OF PIXEL DRAWING
		end
  end // end of clock tick always() block
  
endmodule