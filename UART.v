//
// UART module  - simple async interface
// NJM June 2013
//
// UART(clock,rcvbit,rcvready,rcvread,rcvword,xmitbit,xmitready,xmitsend,xmitword)
//      clock (input) - 50 MHz clock
//      rcvbit (input) - RS232 signal coming into FPGA
//      rcvready (output) - indicates a word has been received
//      rcvread (input) - indicates we have read the received word
//      rcvword[7:0] (output) - 8-bit word received from RS232 line
//      xmitbit (output) - bit sent to RS232 line
//      xmitready (output) - SET when UART is ready to send
//      xmitsend (input) - set to request transmission of xmitword
//      xmitword[7:0] (input) - 8-bit word we're asking to send

module UART(clock,rcvbit,rcvready,rcvread,rcvword,xmitbit,xmitready,xmitsend,xmitword);
  input clock,rcvbit,rcvread,xmitsend;
	input [7:0] xmitword;
	output rcvready,xmitbit,xmitready;
	output [7:0]rcvword;

	wire clock,rcvbit,rcvread,xmitsend;
	wire [7:0]xmitword;			// word to transmit
	reg	rcvready,xmitbit;
	wire xmitready;
	wire [7:0]rcvword;

  reg [15:0]counter;
	reg newword;				// 1 means we're waiting to start receiving a word
	wire trigger;				// drops when we need to begin a word or sample the input
	reg [3:0]bitnum;
  reg [9:0]word;			// saves the word we're receiving (LSB first)
	
	reg xmit;						// 1=sending, 0=idle
	reg [3:0] xbitnum;	// # of bit we're transmitting
	reg [15:0] xclock;	// transmit clock

	assign xmitready=~xmit;	// 1=CTS, 0=BUSY
	assign rcvword=word[7:0];

// If we're waiting to begin a word, drop trigger when our input drops (start bit)
// otherwise drop trigger everytime our counter's MSB resets to 0
	assign trigger=newword?rcvbit:counter[15];	// active low

// we always run the counter
  always @(negedge clock) begin
		if (newword) begin	// set to one + half a bit length
			counter<=65536 - 7812;	// 9600: 50M - 1.5*bit length (skip start it + half of bit 0)
			//counter<=65536-650;	// 115200
			//counter<=65536-1301;	// 57600
		end else if (counter==0) begin
			counter<=65536 - 5207;		// 9600: 50M - bit length (@9600 baud)
			//counter<=65536-433; // @115,200
			//counter<=65536-867;	// 57600
		end else begin
			counter<=counter+16'b1;
		end
  end

// When trigger drops, we need to either begin or continue reading/storing input bits
	always @(negedge trigger) begin
		if (newword) begin	// start a new word
			newword<=0;				// remember we're inside a word
			bitnum<=0;				// # of bits received
		end else begin			// save another bit
			word[bitnum]<=rcvbit;
			bitnum<=bitnum+4'b1;
			if (bitnum==8) begin	// done receiving entire word
				newword<=1;
			end;
		end
	end

// tell the world we have a word ready
// read signal clears the flag
	always @(posedge newword,posedge rcvread) begin
		if (rcvread) begin		// user is reading word; clear flag
			rcvready<=0;
		end else begin		// newword must have just been set
			rcvready<=1;
		end
	end



// transmission code

	always @(/*posedge xmitsend, */negedge clock) begin
		if (xmitsend && (~xmit)) begin	// time to start a transmission
			xmit<=1;				// show we're doing a transmission
			xclock<=0;
			xbitnum<=0;
			xmitbit<=0;			// start bit
		end

	 if (xmit) begin		// we're transmitting here
    if (xclock< 5208) begin	// inside a bit period (9600)
    //if (xclock< 868) begin	// inside a bit period (57600)
			xclock<=xclock+16'b1;
		end else begin	// bit period has finished
			if (xbitnum == 9) begin	// ALL BITS FINISHED! // will 8 work??? %%%
				xmit<=0;	// done transmitting
				xbitnum<=0;
				xclock<=0;
				xmitbit<=1;	// STOP bit
			end else begin	// send next bit
				xbitnum<=xbitnum+4'b1;
				xclock<=0;
// send data bit or STOP bits
				xmitbit<=(xbitnum<=7)?xmitword[xbitnum]:1'b1;  // old bitnum value!
			end
		end	// end of bit period finished block
	 end	// end of IF XMIT block
	end	// end of negedge clock block

endmodule