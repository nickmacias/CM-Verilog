//
// Better CM driver. Break the display into zones around each cell's
// display area, and track these zones during the hpos/hline update codes.
// This should be more HW-efficient than calculating zones from hpos/hline,
// especially when dimensions aren't integral powers of 2...
//
// Read key commands and modulate a cell's inputs.
// Meanwhile, display on the VGA :)
//
module cmtest5(clock,from2102,to2102,red,green,blue,hsync,vsync);

// VGA vars
  output red, green, blue, hsync, vsync;
  wire clock;
  reg [4:0] red;
  reg [3:0] green;
  reg [4:0] blue;
  reg hsync;
	reg vsync;

	reg [12:0] hpos;	// #ticks in current hline (=hline time*50)
	reg  [9:0] hline; // line #(includes blanks and syncs)
  reg [25:0] ttime; // #ticks since first visible line/first visible pixel

// VGA zone variables
	reg [4:0] hpix,vpix;		// largest zone=16 pixels
	reg [4:0] hzone,vzone;	// what zone are we in?

	reg [4:0] hcell,vcell;	// cell #s (column, row)

// CM vars
	reg dn[1:0],ds[1:0],dw[1:0],de[1:0],cn[1:0],cs[1:0],cw[1:0],ce[1:0],cmreset,cmclock;
	wire dno[1:0],dso[1:0],dwo[1:0],deo[1:0],cno[1:0],cso[1:0],cwo[1:0],ceo[1:0];
	wire membit[1:0];
	wire[6:0]membitnum;	// choose bit to access

	reg[1:0] side; // 0=n, 1=s, 2=w, 3=e
	reg cd;				// 0=d, 1=c


// Other vars
	input clock,from2102;
	output to2102;

	wire [7:0]rcvword;
	reg [7:0]xword;
  reg rcvread,xsend;
  wire rcvready,xready;

	reg [7:0] a;

	UART myUART(clock,from2102,rcvready,rcvread,rcvword,to2102,xready,xsend,xword);	// instance of UART

// instantiate cells
	cmcell CELL00(dn[0],ds[0],dw[0],dwo[1],cn[0],cs[0],cw[0],cwo[1],
                cmreset,cmclock,dno[0],dso[0],dwo[0],deo[0],cno[0],cso[0],cwo[0],ceo[0],membitnum,membit[0]);

	cmcell CELL01(dn[1],ds[1],deo[0],de[1],cn[1],cs[1],ceo[0],ce[1],
                cmreset,cmclock,dno[1],dso[1],dwo[1],deo[1],cno[1],cso[1],cwo[1],ceo[1],membitnum,membit[1]);
                     
	always @(negedge clock) begin
		if (xsend && ~xready) xsend<=0; // we started transmission of a word, so clear our send request
// Seems we need to and with ~xready, otherwise the send may get cleared too quickly?
		else if (rcvready) begin
// send an ack
			xword<=rcvword;	// echo back
			xsend<=1;

// process the incoming character
			rcvread<=1;	// grab the word

// parse the command
			if (rcvword=="N") side<=0;
			else if (rcvword=="S") side<=1;
			else if (rcvword=="W") side<=2;
			else if (rcvword=="E") side<=3;
			else if (rcvword=="D") cd<=0;
			else if (rcvword=="C") cd<=1;
			else if (rcvword=="t") cmclock<=1;
			else if (rcvword=="y") cmclock<=0;
			else if (rcvword=="R") cmreset<=1;
			else if (rcvword=="r") begin
				cmreset<=0;
				dn[0]<=0;ds[0]<=0;dw[0]<=0;cn[0]<=0;cs[0]<=0;cw[0]<=0;
				dn[1]<=0;ds[1]<=0;de[1]<=0;cn[1]<=0;cs[1]<=0;ce[1]<=0;
			end else if (rcvword=="1") begin
				case ((cd<<2)|side)
					0: dn[0]<=1;
					1: ds[0]<=1;
					2: dw[0]<=1;
					//3: de[0]<=1;
					4: cn[0]<=1;
					5: cs[0]<=1;
					6: cw[0]<=1;
					//7: ce[0]<=1;
				endcase
			end else if (rcvword=="0") begin
				case ((cd<<2)|side)
					0: dn[0]<=0;
					1: ds[0]<=0;
					2: dw[0]<=0;
					//3: de[0]<=0;
					4: cn[0]<=0;
					5: cs[0]<=0;
					6: cw[0]<=0;
					//7: ce[0]<=0;
				endcase
			end
		end else begin
			rcvread<=0;
		end
	end

// VGA interface
// one clock tick=20 nSec=0.02 uSec (50 ticks per uS)

	assign membitnum=((vzone-1)<<3) | (hzone-1);
  always @(negedge clock) begin
	
		if (hpos > 1033) begin	// end of hsweep
			hpos<=0;
			hzone<=0;hpix<=0;
			hcell<=0;
			if (hline > 806) begin	// end of frame
				hline<=0;
				vzone<=0;vpix<=0;
				vcell<=0;
			end else begin
				hline<=hline+1;
				// adjust vertical zone and pixel
				if (vpix==7) begin
					vpix<=0;
					if (vzone==19) begin
						vzone<=0;
						vcell<=vcell+1;
					end	else vzone<=vzone+1;
				end else vpix<=vpix+1;
			end
		end else begin
			hpos<=hpos+1;
			// adjust horizontal zone and pixel
			if (hpix==7) begin
				hpix<=0;
				if (hzone==11) begin
					hzone<=0;
					hcell<=hcell+1;
				end	else hzone<=hzone+1;
			end else hpix<=hpix+1;
		end

// now drive syncs and bits based on hline/hpos

// hsync first
		if ((hpos >= 806) && (hpos < 910)) begin	// hsync
			hsync <= 0;
		end else begin
			hsync <= 1;
		end

// vsync
		if ((hline >= 771) && (hline < 777)) begin	// vsync
			vsync<=0;
		end else begin
			vsync<=1;
		end

// set databits here
		if (hline >= 768) begin
				red<=0;green<=0;blue<=0;
		end else if (hpos > 787) begin
				red<=0;green<=0;blue<=0;
		end else begin

// START OF PIXEL DRAWING
// Go crazy! Lite some pixels!

if (hcell>=0 && hcell<=1 && vcell==0) begin
// border
			if ((hzone>=1 && hzone<=8 && vzone==1 && vpix==0) ||
			    (hzone>=1 && hzone<=8 && vzone==16 && vpix==7) ||
			    (vzone>=1 && vzone<=16 && hzone==1 && hpix==0) ||
			    (vzone>=1 && vzone<=16 && hzone==8 && hpix==7))
			begin
				red<=31;green<=15;blue<=31;
// edges: outputs
			end else if (hzone == 2 && vzone==0) begin
				red<=0;green<=0;
				blue<=(dno[hcell] && (hpix<<1)>7-vpix && (hpix<<1) < 7+vpix)?31:0;	// funky arrows :-P
			end else if (hzone==3 && vzone==0) begin
				blue<=0;green<=0;
				red<=(cno[hcell] && (hpix<<1)>7-vpix && (hpix<<1) < 7+vpix)?31:0;
			end else if (hzone==6 && vzone==17) begin
				red<=0;green<=0;
				blue<=(dso[hcell] && (hpix<<1)>vpix && (hpix<<1) < 14-vpix)?31:0;
			end else if (hzone==7 && vzone==17) begin
				blue<=0;green<=0;
				red<=(cso[hcell] && (hpix<<1)>vpix && (hpix<<1) < 14-vpix)?31:0;
			end else if (hzone==0 && vzone==14) begin
				red<=0;green<=0;
				blue<=(dwo[hcell] && (vpix<<1)>7-hpix && (vpix<<1)<7+hpix)?31:0;
			end else if (hzone==0 && vzone==15) begin
				blue<=0;green<=0;
				red<=(cwo[hcell] && (vpix<<1)>7-hpix && (vpix<<1)<7+hpix)?31:0;
			end else if (hzone==9 && vzone==2) begin
				red<=0;green<=0;
				blue<=(deo[hcell] && (vpix<<1)>hpix && (vpix<<1)<14-hpix)?31:0;
			end else if (hzone==9 && vzone==3) begin
				blue<=0;green<=0;
				red<=(ceo[hcell] && (vpix<<1)>hpix && (vpix<<1)<14-hpix)?31:0;

// TT bits
			end else if (hzone >= 1 && hzone <= 8 && vzone>=1 && vzone<=16) begin
				blue<=0;
				red<=0;
				green<=membit[hcell]?15:0;	// if we say membitnum<= then we'll be using the old membit here...
			end else begin // off-grid
				blue<=0;
				red<=0;
				green<=0;
			end;
// END OF PIXEL DRAWING
end else begin red<=0;green<=0;blue<=0;end	// not a valid cell location

		end
  end // end of clock tick always() block
  
endmodule